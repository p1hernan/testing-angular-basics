import { compute } from './compute';

describe('compute', () => {

  it('should return 0 if input is negative', () => {
    const resut = compute(-1);
    expect(resut).toBe(0);
  });

  it('should increment the input if it is positive', () => {
    const n = 1;
    const result = compute(n);
    expect(result).toBe(n+1);
  })

});
