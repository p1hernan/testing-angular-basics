import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/empty';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let service: TodoService;

  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  it('should set todos property with the items returned from the server', () => {
    let list = [1, 2, 3];
    spyOn(service, "getTodos").and.callFake(() => {
      return Observable.from([ list ]);
    });

    component.ngOnInit();

    expect(component.todos).toBe(list);
  });

  it("should call the server to save the changes when a new todo item is added", () => {
    let todo = { id: 1 }
    let spy = spyOn(service, "add").and.returnValue(Observable.from([ todo ]));
    component.add();

    expect(component.todos.indexOf(todo)).toBeGreaterThan(-1);
  });

  it("should call the server to save the changes when a new todo item is added", () => {
    let todo = { id: 1 }
    let spy = spyOn(service, "add").and.returnValue(Observable.from([ todo ]));
    component.add();

    expect(component.todos.indexOf(todo)).toBeGreaterThan(-1);
  });

  it("should set the message property if server returnes an error when adding a new todo", () => {
    let message = 'Error del servidor';
    let spy = spyOn(service, "add").and.returnValue(Observable.throw(message));
    component.add();

    expect(component.message).toBe(message);
  });

  it("should call the server to delete a todo item if the user confirms", () => {
    let message = 'Error del servidor';

    spyOn(window, "confirm").and.returnValue(true);
    let spy = spyOn(service, "delete").and.returnValue(Observable.empty());

    component.delete(1);

    expect(spy).toHaveBeenCalled()
  });

  it("should not call the server to delete a todo item if the user confirms", () => {
    let message = 'Error del servidor';

    spyOn(window, "confirm").and.returnValue(false);
    let spy = spyOn(service, "delete").and.returnValue(Observable.empty());

    component.delete(1);

    expect(spy).not.toHaveBeenCalled()
  });
});
